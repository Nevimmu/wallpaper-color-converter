# TODO :
- [ ] Optimize code (so that it doesn't takes a night to render)
- [ ] Use arguments
- [ ] Get color theme from .Xresources or from given argument
- [ ] Publish to PyPi
- [ ] Add wallpaper setter support 