from PIL import ImageColor, Image
from math import sqrt

colorsDict = {}
COLORS = ("#26233A","#EB6F92","#9CCFD8","#F6C177","#31748F","#C4A7E7","#EA9A97","#E0DEF4")
STEPS = 50

wp = "./image.png"
newWp = "./imageNew.png"

def gradient(c1, c2, steps, stepNb):
	"""
	gradient return the RGB value between 2 given colors

	c1 : First color
	c2 : Second color
	steps : Number total of steps
	stepNb : Step number
	"""
	r1, g1, b1 = ImageColor.getcolor(c1, "RGB")
	r2, g2, b2 = ImageColor.getcolor(c2, "RGB")

	r = round(((steps-stepNb)/steps)*r1+(stepNb/steps)*r2)
	g = round(((steps-stepNb)/steps)*g1+(stepNb/steps)*g2)
	b = round(((steps-stepNb)/steps)*b1+(stepNb/steps)*b2)

	return r, g, b

def colorList(colors):
	"""
	colorList get the list of colors and return a list of pairs

	colors : list of colors
	"""
	list = []
	for i in range(len(colors)):
		for j in range(i, len(colors)):
			if i != j:
				list.append((colors[i], colors[j]))
	
	return list

def createGradient(fileName, colorsPairsList, steps):
	"""
	createGradient create a gradient image and color list
	
	fileName : image name
	colorsPairsList : list of colors pair
	steps : number of steps chosen for the gradient
	"""
	im = Image.new("RGB", size=(steps, len(colorsPairsList)))
	themeColorList = []

	for x, color in enumerate(colorsPairsList):
		for y in range(0, steps):
			r, g, b = gradient(color[0], color[1], steps, y)
			im.putpixel((y, x), (r, g, b))
			themeColorList.append((r, g, b))

	return themeColorList

def closestColor(color, colorList):
	"""
	closestColor return the closest color from a given list

	color : color to find the closest color of
	colorList : list of colors to choose from 
	"""
	global colorsDict
	if color not in colorsDict:
		colorDiffs = []
		r, g, b = color
		for themeColor in colorList:
			cr, cg, cb = themeColor
			colorDiff = sqrt(abs(r-cr)**2+abs(g-cg)**2+abs(b-cb)**2)
			colorDiffs.append((colorDiff, themeColor))
		colorsDict[color] = min(colorDiffs)[1]

	return colorsDict[color]

def convertWallpaper(themeColorList, wallpaper):
	""""
	convertWallpaper convert a given image colors to the colors from the themeColorList

	themeColorList : list of colors
	wallpaper : input image
	"""
	wallpaper = Image.open(wallpaper)
	wallpaperData = list(wallpaper.getdata())
	listUniqueColors = list(dict.fromkeys(wallpaper.getdata()))
	convertedColors = convertListOfColors(listUniqueColors, themeColorList)

	newWallpaper = Image.new("RGB", wallpaper.size)
	newWallpaperData = []

	for pixel in wallpaperData:
		print(pixel)
		newPx = convertedColors[pixel]
		newWallpaperData.append(newPx)

	newWallpaper.putdata(newWallpaperData)

	return newWallpaper

def convertListOfColors(list, palette):
	"""
	convertListOfColors create a dictionary with color and converted color

	list : list of colors
	palette : list of colors used to convert
	"""
	convertedDict = {}

	for color in list:
		convertedDict[color] = closestColor(color, palette)
	print("done")
	return convertedDict

def main():
	colorPairsList = colorList(COLORS)
	themeColorList = createGradient("gradientMap", colorPairsList, STEPS)
	newWallpaper = convertWallpaper(themeColorList, wp)

	newWallpaper.save(newWp)
	print("Done")

if __name__ == "__main__":
	main()